<pre>
___  ____         _ _        _____                                           _ 
|  \/  (_)       (_) |      /  __ \                                         | |
| .  . |_ ___ ___ _| | ___  | /  \/ ___  _ __ ___  _ __ ___   __ _ _ __   __| |
| |\/| | / __/ __| | |/ _ \ | |    / _ \| '_ ` _ \| '_ ` _ \ / _` | '_ \ / _` |
| |  | | \__ \__ \ | |  __/ | \__/\ (_) | | | | | | | | | | | (_| | | | | (_| |
\_|  |_/_|___/___/_|_|\___|  \____/\___/|_| |_| |_|_| |_| |_|\__,_|_| |_|\__,_|
</pre>

---

To open and the run project follow these hopefully, but unlikely to be, thorough instructions:

+ Open IntelliJ
+ *File* &xrarr; *New* &xrarr; *Project from existing sources...*
+ Select root folder of this project: **missilecommand_aws241**
+ Select **Import project from external model** and **Gradle**

The above (should) ensure that `gradlew run` works. If not, follow the steps below:

+ *File* &xrarr; *Project Structure...* &xrarr; *Project* &xrarr; *Project SDK*
+ Select **Java 1.8** and *Project Language Level* as **8**
+ *Run* &xrarr; *Edit configurations...* &xrarr; *+* &xrarr; *Application*
+ *Use classpath of module:* **missilecommand.main**
+ *Main class:* **game.base.GameBase**
+ *Name:* **Missile Command**
+ *Run* &xrarr; *Run 'Missile Command'*

---

### How to Play

Your aim is to shoot down the incoming missiles before they destroy your cities. The game will end when all of your cities are destroyed.

Use the mouse to aim and `A`, `S`, and `D` to fire missiles from your three respective silos. The missiles will explode when they reach the location of your mouse pointer at launch.

---

### Description
This was the first of the two bonus projects that I worked on, and so it had the most time put into it. A lot of this was spent rewriting the provided code from the mandatory assignments such that only the Colour class remains unchanged. This was necessary both because I was no longer rendering everything on a grid, and because I wanted to find out how to use the Processing library myself.

#### New Code

##### engine.SoundEngine
The sound engine uses an external library called Minim which was designed to work with Processing. I'm aware that processing has it's own sound library, however it didn't handle some of the files I wanted to use properly, and Minim was well recommended as an alternative.

##### engine.Renderer
The renderer I wrote as a layer beneath GameBase, it still extends PApplet, but it allowed me to break up the visual code from the logic code. It mostly just acts as a wrapper for Processing's drawing calls from the context of the game (e.g. drawMissile() essentially just calls circle() and line()).

##### engine.graphics.GUI
The GUI class works quite well, and I feel quite good about how the related Button class turned out, however I'm not sure it would scale into anything larger. Although I think Button remains relatively generic, the GUI class itself is basically just a giant Magic Number.  

##### game.base.GameBase
This acts as the core application - it's both the start point of main and the persistent class across new games. Its main role is to keep updates happening at a consistent rate - details can be found inside the class itself.

##### game.logic.GameLogic
GameLogic still acts as the authority for the game, and also sets up the playing field. If I were to work on this further, I would probably wrap a lot of the object instantiation into a "level" class to remove the rather extensive setup for all the cities and silos etc. at the top of the file.

---

Music: "Intro" from "Forsaken in the Neon Synthesis" by "YVM", used under [Creative Commons: CC BY-ND 3.0](https://creativecommons.org/licenses/by-nd/3.0/legalcode)

Sounds: taken from http://freesound.org under [Creative Commons](https://creativecommons.org/publicdomain/zero/1.0/)

Libraries: [Minim Audio Library](http://code.compartmental.net/tools/minim/) under [GNU Lesser GPL](https://www.gnu.org/licenses/lgpl-3.0.en.html)