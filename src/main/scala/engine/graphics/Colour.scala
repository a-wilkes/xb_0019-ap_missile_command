// DO NOT MODIFY FOR BASIC SUBMISSION

package engine.graphics

/** Color in red green blue, where each color value is in the range 0-255 */
case class Colour(red: Float, green: Float, blue: Float, alpha: Float) {

  // This is called on new Color(r, g, b)
  def this(red: Float, green: Float, blue: Float) = this(red, green, blue, 255)

  def linearInterpolation(l: Float, r: Float, t: Float): Float = (1 - t) * l + t * r

  def interpolate(fraction: Float, rhs: Colour): Colour =
    Colour(linearInterpolation(red,   rhs.red,   fraction),
          linearInterpolation(green, rhs.green, fraction),
          linearInterpolation(blue,  rhs.blue,  fraction),
          linearInterpolation(alpha, rhs.alpha, fraction)
    )

}

/** Color companion object */
object Colour {

  // This is called on Color(r, g, b) (without new)
  def apply(red: Float, green: Float, blue: Float): Colour = new Colour(red, green, blue)

  val LawnGreen = Colour(124, 252,   0)
  val DarkGreen = Colour(  0, 100,   0)
  val Black     = Colour(  0,   0,   0)
  val Gray      = Colour(100, 100, 100)
  val Red       = Colour(255,   0,   0)
  val White     = Colour(255, 255, 255)
  val LightBlue = Colour(173, 216, 230)
  val Yellow    = Colour(255, 255,   0)
  val Orange    = Colour(255, 165,   0)
  val Blue      = Colour(  0,   0, 255)
  val Green     = Colour(  0, 255,   0)
  val Purple    = Colour( 70,   0,  70)

}
