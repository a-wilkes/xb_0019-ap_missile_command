package engine.graphics

import engine.Coordinate
import engine.graphics.GUI._


class GUI(screenWidth: Int,
          screenHeight: Int,
          startNewGame: () => Unit,
          changeScheme: () => Unit,
          exitGame: () => Unit) {

  private val widthMiddle: Float  = screenWidth / 2
  private val heightMiddle: Float = screenHeight / 2
  private val centre: Coordinate  = new Coordinate(widthMiddle, heightMiddle)

  private val buttonWidth: Float   = screenWidth / 3
  private val buttonHeight: Float  = buttonWidth / 6
  private val buttonPadding: Float = buttonHeight * 1.618f
  private val yButtonOffset: Float = centre.y - buttonHeight
  private val cornerRadius: Int    = 7

  val titleCentre: Coordinate = new Coordinate(centre.x, screenHeight / 6)
  val title: String  = MainTitleText

  val instructions: String = InstructionsText

  private val start: Button  = createButton(yButtonOffset + buttonPadding * 0, StartButtonText,        startNewGame)
  private val scheme: Button = createButton(yButtonOffset + buttonPadding * 1, ColourSchemeButtonText, changeScheme)
  private val quit: Button   = createButton(yButtonOffset + buttonPadding * 2, QuitButtonText,         exitGame)

  private val buttons: Seq[Button] = List(start, scheme, quit)


  def clickButtonAt(x: Int, y: Int): Unit = {
    buttons.foreach(b => if (b.isUnder(x, y)) b.click())
  }


  def getButtons: Seq[Button] = { buttons }


  private def createButton(originY: Float, text: String, callback: () => Unit): Button = {
    val location: Coordinate = new Coordinate(widthMiddle - buttonWidth / 2, originY - buttonHeight / 2)
    new Button(location, buttonWidth, buttonHeight, cornerRadius, text, callback)
  }
}



object GUI {
  val MainTitleText: String = "Missile Command"
  val InstructionsText: String = "Press 'A', 'S', or 'D' to launch missiles at the mouse location." +
    "\nThe game is over when all your cities are destroyed."
  val StartButtonText: String = "Start Game"
  val ColourSchemeButtonText: String = "Change colour scheme"
  val QuitButtonText: String = "Quit Game"
}
