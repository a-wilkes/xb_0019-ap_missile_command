package engine.graphics

import engine.Coordinate


class Button(val origin: Coordinate,
             val width: Float,
             val height: Float,
             val radius: Int,
             val text: String = "",
             val callback: () => Unit) {

  lazy val centre: Coordinate = new Coordinate((width / 2) + origin.x, (height / 2) + origin.y)
  lazy val fontSize: Float = height / 3
  lazy val fontCentre: Coordinate = new Coordinate(centre.x, centre.y - fontSize / 5)


  def isUnder(x: Float, y: Float): Boolean = {
    def withinWidth: Boolean  = { x >= origin.x && x <= origin.x + width  }
    def withinHeight: Boolean = { y >= origin.y && y <= origin.y + height }

    withinWidth && withinHeight
  }

  def click(): Unit = { callback() }
}
