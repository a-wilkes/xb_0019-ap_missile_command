package engine.graphics

class ColourScheme(val skyColour: Colour,
                   val buttonColour: Colour,
                   val buttonTextColour: Colour,
                   val mainTextColour: Colour,
                   val mainForegroundColour: Colour,
                   val explosionColour: Colour,
                   val missileColour: Colour,
                   val missileTrailColour: Colour) {}
