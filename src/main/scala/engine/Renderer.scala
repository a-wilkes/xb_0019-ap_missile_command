package engine

import engine.graphics.{Button, Colour, ColourScheme, GUI}
import engine.Renderer._
import game.base.GameBase.{HeightInPixels, WidthInPixels}
import game.logic.objects.{Explosion, Missile, Renderables, Structure}
import game.logic.GameLogic
import processing.core.{PApplet, PConstants, PImage}


class Renderer extends PApplet {

  // Iterator.continually() creates an infinite iterator, acting in this case like a circular list
  // The flatten is needed as only one element can be passed into the method (normally a function) and it ensures that the resulting iterator only returns a single ColourScheme at a time instead of a list of schemes
  // https://www.scala-lang.org/files/archive/api/2.12.8/scala/collection/Iterator$.html#continually[A](elem:=%3EA):Iterator[A]
  private val colourSchemes: Iterator[ColourScheme] = Iterator.continually(List(DefaultTheme, PunkTheme, RedTheme)).flatten
  private var currentColourScheme: ColourScheme = colourSchemes.next()


  protected def drawMenu(m: GUI, gl: GameLogic): Unit = {
    render(0, gl, inMenu = true)

    drawTitle(m)
    drawInstructions(m, gl.groundDepth)
    m.getButtons.foreach(b => drawButton(b))
  }


  def changeColourScheme(): Unit = {
    currentColourScheme = colourSchemes.next()
  }


  // deltaTime represents how far into the next frame we are when render is called.
  // This allows predictive interpolation to render objects in (probably) the right place mid-update
  // To avoid artifacts when everything should be stopped (game over) the interpolation is set to 0 in drawMenu
  protected def render(deltaTime: Float, gl: GameLogic, inMenu: Boolean): Unit = {
    setBackgroundColour(currentColourScheme.skyColour)
    renderGround(gl.groundDepth, gl.groundHeight)
    renderObjects(deltaTime, gl.render())

    if (!inMenu) drawScore(gl.getScore, gl.gameIsOver)
    if (gl.gameIsOver && !inMenu) drawGameOverScreen()
  }



  private def renderObjects(deltaTime: Float, r: Renderables): Unit = {
    r.missiles.foreach(m => drawMissile(m, deltaTime))
    r.structures.foreach(s => drawStructure(s))
    r.explosions.foreach(e => drawExplosion(e))
  }



  private def drawText(s: String, p: Coordinate, size: Float, c: Colour): Unit = {
    pushStyle()

    textSize(size)
    setFillColour(c)
    text(s, p.x, p.y)

    popStyle()
  }



  private def drawTextContrast(string: String, p: Coordinate, size: Float): Unit = {
    pushStyle()

    val thickness: Float = size * BaseTextContrastThickness

    textSize(size)
    setFillColour(Colour.Black)
    List((1,0),(-1,0),(0,1),(0,-1)).foreach(t => {
      text(string, p.x + (t._1 * thickness), p.y + t._2 * thickness)
    })

    popStyle()
  }



  private def drawTitle(menu: GUI): Unit = {
    pushStyle()

    drawTextContrast(menu.title, menu.titleCentre, MainTextSize)
    drawText(menu.title, menu.titleCentre, MainTextSize, currentColourScheme.mainTextColour)

    popStyle()
  }



  private def drawInstructions(menu: GUI, groundDepth: Float): Unit = {
    pushStyle()

    val centre: Coordinate = new Coordinate(WidthInPixels / 2, HeightInPixels - (groundDepth / 2))
    drawText(menu.instructions, centre, MainTextSize / 4, currentColourScheme.buttonTextColour)

    popStyle()
  }



  private def drawButton(b: Button): Unit = {
    pushStyle()

    setFillColour(currentColourScheme.buttonColour)
    drawRectangle(b.origin, b.width, b.height, b.radius)
    drawText(b.text, b.fontCentre, b.fontSize, currentColourScheme.buttonTextColour)

    popStyle()
  }



  private def drawGameOverScreen(): Unit = {
    pushStyle()

    val mainTextCentre: Coordinate   = new Coordinate(WidthInPixels / 2, (HeightInPixels / 2) - MainTextSize)
    val mainText: String = "GAME OVER!"
    val returnTextCentre: Coordinate = new Coordinate(WidthInPixels / 2, (HeightInPixels / 2) + MainTextSize)
    val returnText: String = "Click to return to the main menu"

    drawTextContrast(mainText, mainTextCentre, MainTextSize)
    drawText(mainText, mainTextCentre, MainTextSize, currentColourScheme.mainTextColour)

    drawTextContrast(returnText, returnTextCentre, GameOverTextSize / 2)
    drawText(returnText, returnTextCentre, GameOverTextSize / 2, currentColourScheme.mainTextColour)

    popStyle()
  }



  private def drawScore(score: Int, gameIsOver: Boolean): Unit = {
    if (gameIsOver) drawGameOverScore(score) else drawInGameScore(score)
  }


  private def drawInGameScore(score: Int): Unit = {
    pushStyle()

    val size: Int = InGameScoreSize
    val text: String = "Score: " + score.toString
    val position: Coordinate = new Coordinate(size, size * 2)

    textAlign(PConstants.LEFT)
    drawText(text, position, size, currentColourScheme.mainTextColour)

    popStyle()
  }


  private def drawGameOverScore(score: Int): Unit = {
    pushStyle()

    val size: Int = GameOverTextSize
    val text: String = "Score: " + score.toString
    val position: Coordinate = new Coordinate(WidthInPixels / 2, HeightInPixels / 2)

    drawTextContrast(text, position, size)
    drawText(text, position, size, currentColourScheme.mainTextColour)

    popStyle()
  }



  private def renderGround(groundDepth: Float, groundHeight: Float): Unit = {
    pushStyle()

    val origin: Coordinate = new Coordinate(0, groundHeight)

    setFillColour(currentColourScheme.mainForegroundColour)
    drawRectangle(origin, WidthInPixels, groundDepth)

    popStyle()
  }



  private def drawStructure(s: Structure): Unit = {
    pushStyle()

    val structureSprite: PImage = loadImage(s.spritePath)

    val imageCentre: Coordinate = new Coordinate(structureSprite.width / 2, structureSprite.height)
    val origin: Coordinate = new Coordinate(s.position.x - imageCentre.x, s.position.y - imageCentre.y)

    setTintColour(currentColourScheme.mainForegroundColour)
    drawImage(structureSprite, origin)

    popStyle()
  }



  private def drawMissile(m: Missile, deltaTime: Float): Unit = {
    pushStyle()

    val offset: Float = deltaTime * m.speed
    val interpolatedPosition: Coordinate = new Coordinate(m.position.x + offset, m.position.y + offset)

    setFillColour(currentColourScheme.missileColour)
    drawMissileTrail(m.origin, interpolatedPosition)
    drawCircle(interpolatedPosition, m.radius * 2)

    popStyle()
  }



  private def drawMissileTrail(origin: Coordinate, position: Coordinate): Unit = {
    pushStyle()

    setStrokeColour(currentColourScheme.missileTrailColour)
    strokeWeight(TrailWidth)
    drawLine(origin, position)

    popStyle()
  }



  private def drawExplosion(e: Explosion): Unit =
    if (e.isFading) drawFadingExplosion(e) else drawGrowingExplosion(e)



  private def drawGrowingExplosion(e: Explosion): Unit = {
    pushStyle()

    setFillColour(currentColourScheme.explosionColour)
    drawCircle(e.location, e.progress * e.radius * 2)

    drawFadingMissileTrail(e.origin, e.location, e.progress)

    popStyle()
  }



  private def drawFadingExplosion(e: Explosion): Unit = {
    pushStyle()

    val c: Colour = getColourWithAlpha(currentColourScheme.explosionColour, e.progress)

    setFillColour(c)
    drawCircle(e.location, e.progress * e.radius * 2)

    popStyle()
  }



  private def drawFadingMissileTrail(origin: Coordinate, position: Coordinate, progress: Float): Unit = {
    pushStyle()

    val c: Colour = getColourWithAlpha(currentColourScheme.missileTrailColour, 1.0f - progress)

    setStrokeColour(c)
    strokeWeight(TrailWidth)
    drawLine(origin, position)

    popStyle()
  }


  private def getColourWithAlpha(c: Colour, r: Float): Colour = { Colour(c.red, c.green, c.blue, r * 255) }

  private def setBackgroundColour(c: Colour): Unit = { background(c.red, c.green, c.blue, c.alpha) }
  private def setFillColour(c: Colour): Unit       = { fill(c.red, c.green, c.blue, c.alpha) }
  private def setStrokeColour(c: Colour): Unit     = { stroke(c.red, c.green, c.blue, c.alpha) }
  private def setTintColour(c: Colour): Unit       = { tint(c.red, c.green, c.blue) }

  private def drawImage(i: PImage, p: Coordinate): Unit          = { image(i, p.x, p.y) }
  private def drawLine(start: Coordinate, end: Coordinate): Unit = { line(start.x, start.y, end.x, end.y) }
  private def drawCircle(p: Coordinate, diameter: Float): Unit   = { circle(p.x, p.y, diameter) }
  private def drawRectangle(p: Coordinate, w: Float, h: Float, edgeRadius: Float = 0): Unit = {
    rect(p.x, p.y, w, h, edgeRadius)
  }
}



object Renderer {
  val BaseTextContrastThickness: Float = 0.05f
  val MainTextSize: Int = 100
  val InGameScoreSize: Int = 25
  val GameOverTextSize: Int = MainTextSize / 2

  val TrailWidth: Float = 2.0f

  val DefaultTheme: ColourScheme = new ColourScheme(
    skyColour            = Colour.Black,
    buttonColour         = Colour.White,
    buttonTextColour     = Colour.Black,
    mainTextColour       = Colour.Red,
    mainForegroundColour = Colour.White,
    explosionColour      = Colour.White,
    missileColour        = Colour.Red,
    missileTrailColour   = Colour.Red
  )

  val PunkTheme: ColourScheme = new ColourScheme(
    skyColour            = Colour( 51,  37,  62),
    buttonColour         = Colour(209,  89, 106),
    buttonTextColour     = Colour.Black,
    mainTextColour       = Colour(207, 156,  88),
    mainForegroundColour = Colour(209,  89, 106),
    explosionColour      = Colour(207, 156,  88),
    missileColour        = Colour(207, 156,  88),
    missileTrailColour   = Colour(207, 156,  88)
  )

  val RedTheme: ColourScheme = new ColourScheme(
    skyColour            = Colour.Red,
    buttonColour         = Colour.White,
    buttonTextColour     = Colour.Black,
    mainTextColour       = Colour.White,
    mainForegroundColour = Colour.White,
    explosionColour      = Colour.Black,
    missileColour        = Colour.White,
    missileTrailColour   = Colour.Black
  )
}
