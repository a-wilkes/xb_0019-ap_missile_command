package engine

import ddf.minim.{AudioPlayer, AudioSample, Minim}
import game.base.GameBase
import engine.SoundEngine._



class SoundEngine(gameBase: GameBase) {

  val minim = new Minim(gameBase)

  var music: Option[AudioPlayer] = None
  var missileExplosion: Option[AudioSample] = None
  var structureExplosion: Option[AudioSample] = None
  var icbmLaunch: Option[AudioSample] = None



  def setup(): Unit = {
    music              = Option(minim.loadFile(MusicFilePath))
    missileExplosion   = Option(minim.loadSample(MissileExplosionFilePath))
    structureExplosion = Option(minim.loadSample(StructureExplosionFilePath))
    icbmLaunch         = Option(minim.loadSample(IcbmLaunchFilePath))

    playMusic()
  }


  def playMissileExplosion(): Unit   = playSounds(missileExplosion)
  def playStructureExplosion(): Unit = playSounds(structureExplosion)
  def playIcbmLaunch(): Unit         = playSounds(icbmLaunch)


  private def playSounds(s: Option[AudioSample]): Unit = {
    s match {
      case Some(m) => m.trigger()
      case None =>
    }
  }


  private def playMusic(): Unit = {
    music match {
      case Some(m) => m.loop()
      case None =>
    }
  }

}



object SoundEngine {
  val MainFilePath: String = "res/sound/"
  val MusicFilePath: String = MainFilePath + "forsaken.mp3"
  val MissileExplosionFilePath: String = MainFilePath + "missile_explosion.wav"
  val StructureExplosionFilePath: String = MainFilePath + "structure_explosion.wav"
  val IcbmLaunchFilePath: String = MainFilePath + "icbm_launch.wav"
}
