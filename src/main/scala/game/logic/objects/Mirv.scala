package game.logic.objects

import engine.Coordinate
import game.logic.objects.Mirv._


class Mirv(val randomRatio: Float,
           override val origin: Coordinate,
           override val target: Coordinate,
           override val position: Coordinate,
           override val radius: Int,
           override val speed: Float,
           override val explosionSoundCallback: () => Unit)
  extends Missile(origin, target, position, radius, speed, explosionSoundCallback) {

  val retargetPoint: Coordinate = findRetargetPoint()

  private val retargetDistanceFromOrigin: Float = distanceFromOrigin(retargetPoint)


  override def update(): Mirv = {
    if (distanceFromOrigin(position) >= targetDistanceFromOrigin) stop(target)
    else new Mirv(randomRatio, origin, target, newLocation, radius, speed, explosionSoundCallback)
  }

  def readyToRetarget(): Boolean = { distanceFromOrigin(position) >= retargetDistanceFromOrigin }

  override def stop(location: Coordinate = newLocation): Mirv =
    new Mirv(randomRatio, origin, target, location, radius, StoppedSpeed, explosionSoundCallback)


  def findRetargetPoint(): Coordinate = {
    val ratio: Float = MirvMinRetargetHeight + (randomRatio * MirvMaxRetargetHeight)
    def getPointOnLine(p1: Float, p2: Float): Float = (ratio * p1) + ((1 - ratio) * p2)

    new Coordinate(getPointOnLine(origin.x, target.x), getPointOnLine(origin.y, target.y))
  }

  def mirvToMissile(missileRadius: Int, newExplosionSoundCallback: () => Unit): Missile =
    new Missile(origin, target, position, missileRadius, speed, newExplosionSoundCallback)
}


object Mirv {
  private val StoppedSpeed: Float = 0.0f

  private val MirvMinRetargetHeight: Float = 0.35f
  private val MirvMaxRetargetHeight: Float = 0.9f - MirvMinRetargetHeight
}
