package game.logic.objects

import engine.Coordinate
import game.logic.objects.Silo._

class Silo(override val position: Coordinate,
           val launchCallback: (Coordinate, Float, Boolean) => Unit,
           val destructionSoundCallback: () => Unit,
           val launchSoundCallback: () => Unit,
           override val destroyed: Boolean = false,
           val cooldown: Int = 0)
  extends Structure {

  override protected val undamagedSpritePath: String = UndamagedSpritePath
  override protected val destroyedSpritePath: String = DestroyedSpritePath


  def destroy(): Silo = {
    destructionSoundCallback()
    new Silo(position, launchCallback, destructionSoundCallback, launchSoundCallback, destroyed = true, cooldown)
  }

  def update(): Silo =
    new Silo(position, launchCallback, destructionSoundCallback, launchSoundCallback, destroyed, (cooldown - 1).max(0))

  def ableToLaunch(): Boolean =
    !destroyed && cooldown == 0

  def launch(target: Coordinate): Silo = {
    launchSoundCallback()
    launchCallback(target, SiloMissileSpeed, true)
    new Silo(position, launchCallback, destructionSoundCallback, launchSoundCallback, destroyed, DefaultSiloCooldown)
  }
}


object Silo {
  private val DefaultSiloCooldown: Int = 100

  private val SiloMissileSpeed: Float = 5.0f

  private val UndamagedSpritePath: String = "res/silo.png"
  private val DestroyedSpritePath: String = "res/silo_destroyed.png"
}
