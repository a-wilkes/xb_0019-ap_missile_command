package game.logic.objects


class Renderables(val missiles: Seq[Missile],
                  val explosions: Seq[Explosion],
                  val structures: Seq[Structure])