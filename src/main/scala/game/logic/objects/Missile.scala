package game.logic.objects

import engine.Coordinate
import game.logic.objects.Missile._


class Missile(val origin: Coordinate,
              val target: Coordinate,
              val position: Coordinate,
              val radius: Int,
              val speed: Float,
              val explosionSoundCallback: () => Unit) {

  protected val targetDistanceFromOrigin: Float = distanceFromOrigin(target)
  protected val xDirection: Float = (target.x - origin.x) / targetDistanceFromOrigin
  protected val yDirection: Float = (target.y - origin.y) / targetDistanceFromOrigin



  protected def distanceFromOrigin(c: Coordinate): Float =
    math.sqrt(math.pow(c.x - origin.x, 2) + math.pow(c.y - origin.y, 2)).toFloat


  protected def newLocation: Coordinate =
    new Coordinate(position.x + (xDirection * speed),
                   position.y + (yDirection * speed))


  def update(): Missile = {
    if (distanceFromOrigin(position) >= targetDistanceFromOrigin) stop(target)
    else new Missile(origin, target, newLocation, radius, speed, explosionSoundCallback)
  }


  def detonate(): Explosion = {
    explosionSoundCallback()
    new Explosion(position, origin)
  }


  def stop(location: Coordinate = newLocation): Missile =
    new Missile(origin, target, location, radius, StoppedSpeed, explosionSoundCallback)


  def isStopped: Boolean =
    speed == StoppedSpeed
}



object Missile {
  private val StoppedSpeed: Float = 0.0f
}
