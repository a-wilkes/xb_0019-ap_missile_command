package game.logic.objects

import engine.Coordinate


abstract class Structure {

  val position: Coordinate
  val destroyed: Boolean

  protected val undamagedSpritePath: String
  protected val destroyedSpritePath: String


  def destroy(): Structure


  def spritePath: String =
    if (destroyed) destroyedSpritePath else undamagedSpritePath
}
