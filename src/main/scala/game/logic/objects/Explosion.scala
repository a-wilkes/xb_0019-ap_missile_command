package game.logic.objects

import engine.Coordinate
import game.logic.objects.Explosion._

class Explosion(val location: Coordinate,
                val origin: Coordinate,
                val radius: Int = DefaultRadius,
                val progress: Float = DefaultProgress,
                val rate: Float = DefaultRate) {


  def update(): Explosion = {
    val newRate = if (reachedMaxSize()) -rate else rate
    val newProgress = progress + (newRate / progress)

    new Explosion(location, origin, radius, newProgress, newRate)
  }

  private def reachedMaxSize(): Boolean =
    progress > MaxProgress && !isFading

  def isOver: Boolean =
    progress <= MinProgress

  def isFading: Boolean =
    rate < 0.0f
}

object Explosion {
  private val DefaultRadius: Int = 75
  private val DefaultProgress: Float = 0.01f
  private val MaxProgress: Float = 1.0f
  private val MinProgress: Float = 0.0f
  private val DefaultRate: Float = 0.004f
}
