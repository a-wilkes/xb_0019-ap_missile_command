package game.logic.objects

import engine.Coordinate
import game.logic.objects.City._

class City(override val position: Coordinate,
           val destructionSoundCallback: () => Unit,
           override val destroyed: Boolean = false)
  extends Structure {

  override protected val undamagedSpritePath: String = UndamagedSpritePath
  override protected val destroyedSpritePath: String = DestroyedSpritePath

  def destroy(): City = {
    destructionSoundCallback()
    new City(position, destructionSoundCallback, destroyed = true)
  }
}

object City {
  private val UndamagedSpritePath: String = "res/city.png"
  private val DestroyedSpritePath: String = "res/city_destroyed.png"
}
