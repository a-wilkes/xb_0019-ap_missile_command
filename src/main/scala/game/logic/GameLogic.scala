package game.logic

import java.awt.event.KeyEvent._

import engine.{Coordinate, SoundEngine}
import game.logic.objects.{City, Explosion, Mirv, Missile, Renderables, Silo, Structure}
import game.logic.GameLogic._
import processing.event.KeyEvent

import scala.util.Random


class GameLogic(Width: Int, Height: Int, soundEngine: SoundEngine) {

  private var icbms: Vector[Missile]        = Vector()
  private var mirvs: Vector[Mirv]           = Vector()
  private var explosions: Vector[Explosion] = Vector()


  private var score: Float = 0


  private var updatesUntilIcbmLaunch: Int  = getNextLaunchTime(IcbmMinFrequency, IcbmMaxFrequency)
  private var icbmLaunchesUntilMirvLaunch: Int = getNextLaunchTime(MirvMinFrequency, MirvMaxFrequency)


  def groundDepth: Int  = GroundDepth
  val groundHeight: Int = Height - groundDepth


  private val siloPadding: Int = Width / 8

  private val siloLeftPosition: Coordinate   = new Coordinate(siloPadding,         groundHeight)
  private val siloCenterPosition: Coordinate = new Coordinate(Width / 2,           groundHeight)
  private val siloRightPosition: Coordinate  = new Coordinate(Width - siloPadding, groundHeight)

  private val missileLaunchHeight: Int = groundHeight - SiloHeight

  private val leftLaunchPoint: Coordinate   = new Coordinate(siloLeftPosition.x,   missileLaunchHeight)
  private val centreLaunchPoint: Coordinate = new Coordinate(siloCenterPosition.x, missileLaunchHeight)
  private val rightLaunchPoint: Coordinate  = new Coordinate(siloRightPosition.x,  missileLaunchHeight)

  private var silos: Vector[Silo] = Vector(
    new Silo(siloLeftPosition, launchMissile(leftLaunchPoint),
      soundEngine.playStructureExplosion _, soundEngine.playIcbmLaunch _),

    new Silo(siloCenterPosition, launchMissile(centreLaunchPoint),
      soundEngine.playStructureExplosion _, soundEngine.playIcbmLaunch _),

    new Silo(siloRightPosition,  launchMissile(rightLaunchPoint),
      soundEngine.playStructureExplosion _, soundEngine.playIcbmLaunch _)
  )

  private def leftSilo: Silo   = silos(0)
  private def middleSilo: Silo = silos(1)
  private def rightSilo: Silo  = silos(2)


  private val cityPadding: Float  = (siloCenterPosition.x - siloPadding) / 4
  private val cityLeftStart: Float = siloLeftPosition.x
  private val cityRightStart: Float = Width - cityLeftStart

  private var cities: Vector[City] = Vector(
    new City(new Coordinate(cityLeftStart  + cityPadding * 1, groundHeight), soundEngine.playStructureExplosion _),
    new City(new Coordinate(cityLeftStart  + cityPadding * 2, groundHeight), soundEngine.playStructureExplosion _),
    new City(new Coordinate(cityLeftStart  + cityPadding * 3, groundHeight), soundEngine.playStructureExplosion _),
    new City(new Coordinate(cityRightStart - cityPadding * 3, groundHeight), soundEngine.playStructureExplosion _),
    new City(new Coordinate(cityRightStart - cityPadding * 2, groundHeight), soundEngine.playStructureExplosion _),
    new City(new Coordinate(cityRightStart - cityPadding * 1, groundHeight), soundEngine.playStructureExplosion _)
  )



  def update(): Unit = {
    if (gameIsOver) return

    if (icbmLaunchReady()) launchIcbm()

    updateAllObjects()
    retargetMirvs()
    detectCollisions()

    updateScore()
  }


  def updateScore(): Unit = { score += PointsPerUpdate * (1 + silos.count(!_.destroyed)) }
  def getScore: Int = { score.toInt }
  def gameIsOver: Boolean = { cities.forall(_.destroyed) }


  private def updateAllObjects(): Unit = {
    silos = silos.map(_.update())
    icbms = icbms.map(_.update())
    mirvs = mirvs.map(_.update())

    explosions = explosions.map(_.update())
    explosions = explosions.filter(!_.isOver) ++ findNewExplosions()

    icbms = icbms.filter(!_.isStopped)
    mirvs = mirvs.filter(!_.isStopped)
  }


  private def findNewExplosions(): Vector[Explosion] =
    (icbms ++ mirvs).filter(_.isStopped).map(_.detonate())


  private def getNextLaunchTime(min: Int, max: Int): Int =
    min + RandomGenerator.nextInt(max)


  private def icbmLaunchReady(): Boolean = {
    updatesUntilIcbmLaunch = (updatesUntilIcbmLaunch - 1).max(0)

    updatesUntilIcbmLaunch == 0
  }


  private def launchIcbm(): Unit = {
    updatesUntilIcbmLaunch = getNextLaunchTime(IcbmMinFrequency, IcbmMaxFrequency)

    val origin: Coordinate = new Coordinate(RandomGenerator.nextFloat() * Width, 0)
    val target: Option[Coordinate] = findValidTarget()

    if (target.isEmpty) return

    if (icbmLaunchesUntilMirvLaunch == 0) launchMirv(target.get, origin, IcbmMissileSpeed)
    else launchMissile(origin)(target.get, IcbmMissileSpeed)
  }



  private def findValidTarget(remove: Seq[Structure] = Seq()): Option[Coordinate] = {
    val validTargets: Seq[Structure] = (cities.filter(!_.destroyed) ++ silos.filter(!_.destroyed)).diff(remove)

    if (validTargets.isEmpty) return None

    val target: Structure = validTargets(RandomGenerator.nextInt(validTargets.size))
    Some(target.position)
  }


  private def detectCollisions(): Unit = {
    handleMissileCollisions()
    handleCityCollisions()
    handleSiloCollisions()
  }


  private def handleMissileCollisions(): Unit = {
    val destroyedIcbms: Vector[Missile] = (icbms ++ mirvs).filter(i => isInsideExplosion(i.position, i.radius))
    icbms = icbms.diff(destroyedIcbms) ++ destroyedIcbms.map(_.stop())
    mirvs = mirvs.diff(destroyedIcbms)

    score += (PointsPerMissileDestroyed * destroyedIcbms.size)
  }


  private def handleCityCollisions(): Unit = {
    val destroyedCities: Vector[City] = cities.filter(c => !c.destroyed && isInsideExplosion(c.position))
    cities = cities.diff(destroyedCities) ++ destroyedCities.map(_.destroy())
  }


  private def handleSiloCollisions(): Unit = {
    silos = silos.map(s => if (!s.destroyed && isInsideExplosion(s.position)) s.destroy() else s)
  }


  private def isInsideExplosion(c: Coordinate, radius: Float = 0): Boolean = {
    explosions.exists(e => colliding(c, radius, e.location, e.radius * e.progress))
  }


  private def colliding(c1: Coordinate, radius1: Float, c2: Coordinate, radius2: Float): Boolean = {
    val dx = c1.x - c2.x
    val dy = c1.y - c2.y

    Math.sqrt(dx * dx + dy * dy) < radius1 + radius2
  }


  def render(): Renderables = {
    new Renderables(icbms ++ mirvs, explosions, silos ++ cities)
  }


  def keyPressed(event: KeyEvent, x: Float, y: Float): Unit = {
    silos = event.getKeyCode match {
      case VK_A => launchFromSilo(leftSilo)
      case VK_S => launchFromSilo(middleSilo)
      case VK_D => launchFromSilo(rightSilo)
      case _    => silos
    }

    def launchFromSilo(silo: Silo): Vector[Silo] = {
      val target: Coordinate = new Coordinate(x, if (y > groundHeight) groundHeight else y)

      if (silo.ableToLaunch()) silos.map(s => if (s == silo) s.launch(target) else s)
      else silos
    }
  }


  private def launchMissile(origin: Coordinate)(target: Coordinate, speed: Float, sound: Boolean = false): Unit = {
    val missile = new Missile(origin, target, origin, MissileRadius, speed, soundEngine.playMissileExplosion _)

    if (isInsideExplosion(missile.position, missile.radius)) return

    icbms = icbms :+ missile
    icbmLaunchesUntilMirvLaunch = (icbmLaunchesUntilMirvLaunch - 1).max(0)

    if (sound) soundEngine.playIcbmLaunch()
  }


  private def launchMirv(target: Coordinate, origin: Coordinate, speed: Float): Unit = {
    val mirv = new Mirv(RandomGenerator.nextFloat(),
      origin, target, origin, MirvRadius, speed,
      soundEngine.playMissileExplosion _
    )

    if (isInsideExplosion(mirv.position, mirv.radius)) return

    mirvs = mirvs :+ mirv
    icbmLaunchesUntilMirvLaunch = getNextLaunchTime(MirvMinFrequency, MirvMaxFrequency)
  }


  private def retargetMirvs(): Unit = {
    mirvs.filter(_.readyToRetarget()).foreach(m => retargetAndLaunch(m))

    mirvs = mirvs.filter(!_.readyToRetarget())
  }



  private def retargetAndLaunch(m: Mirv): Unit = {
    val invalidTargets: Seq[Structure] = (silos ++ cities).filter(s => s.position == m.target)

    for (_ <- 0 until RandomGenerator.nextInt(2) + 1) {
      val target: Option[Coordinate] = findValidTarget(invalidTargets)

      if (target.isDefined) launchMissile(m.position)(target.get, IcbmMissileSpeed)
    }

    icbms = icbms :+ m.mirvToMissile(MissileRadius, soundEngine.playMissileExplosion _)
  }
}



object GameLogic {

  private val RandomGenerator: Random.type = scala.util.Random

  private val PointsPerUpdate: Float         = 0.01f
  private val PointsPerMissileDestroyed: Int = 5

  private val IcbmMinFrequency: Int = 30
  private val IcbmMaxFrequency: Int = 180
  private val MirvMinFrequency: Int = 4
  private val MirvMaxFrequency: Int = 7

  private val IcbmMissileSpeed: Float = 1.0f

  private val MissileRadius: Int = 2
  private val MirvRadius: Int    = 2 * MissileRadius

  private val GroundDepth: Int = 100

  // SiloHeight is based on the size of the sprite, so hard-coding it here is a little janky, but much easier...
  private val SiloHeight: Int = 40
}

